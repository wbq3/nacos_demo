package com.nacos.demo.wbq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @ClassName NacosDemoApplication
 * @Description TODO
 * @Author wbq
 * @Date 2023/4/8 11:37
 * @Version 1.0
 */
@EnableDiscoveryClient
@SpringBootApplication
public class NacosDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(NacosDemoApplication.class);
    }
}
