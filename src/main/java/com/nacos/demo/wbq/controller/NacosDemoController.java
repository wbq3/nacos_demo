package com.nacos.demo.wbq.controller;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName NacosDemoController
 * @Description TODO
 * @Author wbq
 * @Date 2023/4/8 11:39
 * @Version 1.0
 */

@Api(tags = {"nacos获取配置中心数据"})
@RestController
@RequestMapping("/nacos")
@RefreshScope
public class NacosDemoController {

    @Value("${name}")
    private String userName;

    @Value("${code}")
    private String userCode;

    @ApiOperation(value = "getUserInfo", notes = "获取配置信息")
    @GetMapping("/user")
    public String getUserInfo(){
        return userCode+"："+userName;
    }

}
